Rails.application.routes.draw do

  root 'sessions#index'

  resources :sessions
  resources :users

  resources :clients do
    member do
      get :credits, :deposits, :bills,
      :credit_transactions, :deposit_transactions, :bill_transactions, :transactions,
      :archive_deposits, :archive_credits, :archive_bills,
      :new_bill, :new_credit, :new_deposit
    end
  end


  resources :client_deposits do
    member do
      get :to_archive
      get :new_transaction
    end
  end
  resources :client_credits do
    member do
      get :to_archive
      get :new_transaction
    end
  end


  resources :deposits do
    member do
      get :to_archive
    end
  end
  resources :credits do
    member do
      get :to_archive
    end
  end
  resources :client_bills do
    member do
      get :to_archive
      get :new_transaction
    end
  end


  resources :deposit_operations
  resources :credit_operations
  resources :bill_operations

  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'
  get '/search' => 'clients#index'
  get '/contacts' => 'sessions#contacts'
  get '/trigger' => 'sessions#trigger'
  get '/debtors' => 'client_credits#debtors'


  end
