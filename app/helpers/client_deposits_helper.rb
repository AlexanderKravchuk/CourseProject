module ClientDepositsHelper

  def has_access_to_deposit
    if is_manager?
      return true
    end
    @client_deposit = ClientDeposit.find(params[:id])
    if !(current_user.id == @client_deposit.client_id)
      flash[:notice] = "Access denied!"
      redirect_to '/'
    end
  end

end
