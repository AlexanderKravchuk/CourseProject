module ClientBillsHelper

  def has_access_to_bill
    if is_manager?
      return true
    end
    @client_bill = ClientBill.find(params[:id])
    if !(current_user.id == @client_bill.client_id)
      flash[:notice] = "Access denied!"
      redirect_to '/'
    end
  end

end
