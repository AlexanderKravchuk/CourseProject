module DepositOperationsHelper

  def has_access_to_deposit_operation
    if is_manager?
      return true
    end
    @deposit_operation = DepositOperation.find(params[:id])
    if !(current_user.id == ClientDeposit.find(@deposit_operation.cliend_deposit_id).client_id)
      flash[:notice] = "Access denied!"
      redirect_to '/'
    end
  end
end
