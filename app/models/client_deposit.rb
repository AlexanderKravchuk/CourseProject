class ClientDeposit < ApplicationRecord
  belongs_to :client
  has_many :deposit_operations, dependent: :destroy
  belongs_to :deposit

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/
  validates :sum, presence: true, numericality: true,
  format: { with: VALID_SUM_REGEX }

  validate :check_client
  validate :check_deposit

  private
  def check_deposit
  	errors.add(:deposit_id, "can't find deposit") unless Deposit.exists?(self.deposit_id)
  end

  def check_client
  	errors.add(:client_id, "can't find client") unless Client.exists?(self.client_id)
  end

end
