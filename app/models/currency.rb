class Currency < ApplicationRecord
  has_many :client_bills, dependent: :destroy
  has_many :credits, dependent: :destroy
  has_many :deposits, dependent: :destroy

  VALID_NAME_REGEX = /\A[A-Za-z]{1,20}\z/ # dolalr, euro ...
  validates :name, presence: true, uniqueness: true,
  format: { with: VALID_NAME_REGEX }

  validates :symbol, presence: true, length: { is: 1 } # $ ...
end
