class SessionsController < ApplicationController

  def index
  end

  def new
  end

  def trigger
    	client_credits = ClientCredit.where(:status => true)
    	client_credits.each do |client_credit|
    		if (client_credit[:payment_date] <=> Date.today) == -1
    			penalty = Credit.where(:id => client_credit[:credit_id])[0][:penalty]
        cl_credit = ClientCredit.find(client_credit[:id])
        cl_credit.update(current_sum: (client_credit[:current_sum] + client_credit[:current_sum]*(penalty/100)).round(2))
    		end
    	end
    	client_deposits = ClientDeposit.where(:status => true)
    	client_deposits.each do |client_deposit|
    		rate = Deposit.where(:id => client_deposit[:deposit_id])[0][:rate]
        cl_deposit = ClientDeposit.find(client_deposit[:id])
        cl_deposit.update(sum: (client_deposit[:sum] + client_deposit[:sum]*(1.0/365)*(rate/100)).round(2))
    	end
    flash[:success] = "Successfully ВЖУХ!"
    redirect_to "/"
  end

  def create
    user = User.find_by(login: params[:session][:login])
    if user && user.authenticate(params[:session][:password])
      sign_in(user)
      flash[:success] = "Successfully signed in!"
      redirect_to ''
    else
      flash.now[:error] = "Invalid login/password combination!"
      render 'new'
    end
  end

  def destroy
    sign_out
    flash[:notice] = "Signed out!"
    redirect_to ''
  end

end
