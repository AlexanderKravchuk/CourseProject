class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include ClientDepositsHelper
  include ClientCreditsHelper
  include ClientBillsHelper
  include DepositOperationsHelper
  include CreditOperationsHelper
  include BillOperationsHelper
end
