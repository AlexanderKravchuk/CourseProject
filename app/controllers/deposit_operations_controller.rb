class DepositOperationsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_deposit_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @deposit_operation = DepositOperation.all
  end

  def show
    @deposit_operation = DepositOperation.where(:id => params[:id])
  end

  def create
    @client_deposit = ClientDeposit.find(params[:client_deposit_id])
    @deposit_operation = @client_deposit.deposit_operations.build(deposit_operation_params)
    if @client_deposit.sum + @deposit_operation.sum.round(2) > 0
      if @deposit_operation.save
        if (@deposit_operation.sum > 0 && Deposit.find(@client_deposit.deposit_id).refill) ||
        (@deposit_operation.sum < 0 && Deposit.find(@client_deposit.deposit_id).widthdrawal)
          @client_deposit.update(sum: (@client_deposit.sum + @deposit_operation.sum).round(2))
          flash[:success] = "Successfully added!"
          redirect_to @client_deposit
        else
          flash[:error] = "Incorrect data!"
          redirect_to "/client_deposits/#{params[:client_deposit_id]}/new_transaction"
        end
      else
        flash[:error] = "Incorrect data!"
        redirect_to "/client_deposits/#{params[:client_deposit_id]}/new_transaction"
      end
    elsif @client_deposit.sum + @deposit_operation.sum.round(2) == 0
      flash[:success] = "Added to archive"
      if (@deposit_operation.sum > 0 && Deposit.find(@client_deposit.deposit_id).refill) ||
      (@deposit_operation.sum < 0 && Deposit.find(@client_deposit.deposit_id).widthdrawal)
        @client_deposit.update(sum: (@client_deposit.sum + @deposit_operation.sum).round(2))
        redirect_to "/client_deposits/#{params[:client_deposit_id]}/to_archive"
      else
        flash[:error] = "Incorrect data!"
        redirect_to "/client_deposits/#{params[:client_deposit_id]}/new_transaction"
      end
    else
      flash[:error] = "Incorrect data!"
      redirect_to "/client_deposits/#{params[:client_deposit_id]}/new_transaction"
    end
  end

  private
    def deposit_operation_params
      params.require(:deposit_operation).permit(:sum)
    end

end
