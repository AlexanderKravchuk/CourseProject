class BillOperationsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_bill_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @bill_operation = BillOperation.all
  end

  def show
    @bill_operation = BillOperation.where(:id => params[:id])
  end

  def create
    @client_bill = ClientBill.find(params[:client_bill_id])
    @bill_operation = @client_bill.bill_operations.build(bill_operation_params)
    if @client_bill.sum + @bill_operation.sum.round(2) >= 0
      if @bill_operation.save
        flash[:success] = "Successfully added!"
        @client_bill.update(sum: (@client_bill.sum + @bill_operation.sum).round(2))
        redirect_to @client_bill
      else
        flash[:error] = "Incorrect data!"
        redirect_to "/client_bills/#{params[:client_bill_id]}/new_transaction"
      end
    else
      flash[:error] = "Incorrect data!"
      redirect_to "/client_bills/#{params[:client_bill_id]}/new_transaction"
    end
  end

  private
  def bill_operation_params
    params.require(:bill_operation).permit(:sum)
  end

end
