class ClientCreditsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_credit, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @client_credit = ClientCredit.all
  end

  def debtors
    @client_credit = ClientCredit.where(:status => true)
  end
  
  def show
    @client_credit = ClientCredit.where(:id => params[:id])
    @credit_operation = CreditOperation.where(:client_credit_id => params[:id])
    term = Credit.where(:id => ClientCredit.where(:id => params[:id])[0][:credit_id])[0][:term]
    rate = Credit.where(:id => ClientCredit.where(:id => params[:id])[0][:credit_id])[0][:rate]
    @final_sum = @client_credit[0][:sum] + @client_credit[0][:sum]*(term/365)*(rate/100)
  end

  def create
    @client = Client.find(params[:client_id])
    @client_credit = @client.client_credits.build(client_credit_params)
    if @client_credit.sum > Credit.find(@client_credit.credit_id).max_sum
      flash[:error] = "Incorrect data!"
      redirect_to "/clients/#{params[:client_id]}/new_credit"
    else
      @client_credit.status = true
      term = Credit.where(:id => params[:client_credit][:credit_id])[0][:term]
      rate = Credit.where(:id => params[:client_credit][:credit_id])[0][:rate]
      @client_credit.current_sum = @client_credit.sum + @client_credit.sum*(term/365)*(rate/100)
      @client_credit.payment_date = Date.today.next_day(term)
      if @client_credit.save
        flash[:success] = "Successfully added!"
        redirect_to @client_credit
      else
        flash[:error] = "Incorrect data!"
        redirect_to "/clients/#{params[:client_id]}/new_credit"
      end
    end
  end

  def edit
    @client_credit = ClientCredit.find(params[:id])
  end

  def update
    @client_credit = ClientCredit.find(params[:id])
    if @client_credit.update(client_credit_params)
      flash[:success] = "Successfully updated!"
      redirect_to @client_credit
    else
      render 'edit'
    end
  end

  def to_archive
    @client_credit = ClientCredit.find(params[:id])
    @client_credit.update(status: false)
    flash[:success] = "Added to archive"
    redirect_to "/clients/#{@client_credit.client_id}/credits"
  end

  def new_transaction
    @credit_operation = CreditOperation.new
  end

  private
    def client_credit_params
      params.require(:client_credit).permit(:credit_id, :sum)
    end

end
