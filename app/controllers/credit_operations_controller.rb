class CreditOperationsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_credit_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @credit_operation = CreditOperation.all
  end

  def show
    @credit_operation = CreditOperation.where(:id => params[:id])
  end

  def create
    @client_credit = ClientCredit.find(params[:client_credit_id])
    @credit_operation = @client_credit.credit_operations.build(credit_operation_params)
    if @client_credit.current_sum - @credit_operation.sum.round(2) > 0
      if @credit_operation.save
        @client_credit.update(current_sum: (@client_credit.current_sum - @credit_operation.sum).round(2))
        flash[:success] = "Successfully added!"
        redirect_to @client_credit
      else
        flash[:error] = "Incorrect data!"
        redirect_to "/client_credits/#{params[:client_credit_id]}/new_transaction"
      end
    elsif @client_credit.current_sum - @credit_operation.sum.round(2) == 0
      flash[:success] = "Added to archive"
      @client_credit.update(current_sum: (@client_credit.current_sum - @credit_operation.sum).round(2))
      redirect_to "/client_credits/#{params[:client_credit_id]}/to_archive"
    else
      flash[:error] = "Incorrect data!"
      redirect_to "/client_credits/#{params[:client_credit_id]}/new_transaction"
    end
  end

  private
    def credit_operation_params
      params.require(:credit_operation).permit(:sum)
    end

end
