--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bill_operations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bill_operations (
    id integer NOT NULL,
    client_bill_id integer,
    sum double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: bill_operations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bill_operations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bill_operations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bill_operations_id_seq OWNED BY bill_operations.id;


--
-- Name: client_bills; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE client_bills (
    id integer NOT NULL,
    client_id integer,
    sum double precision,
    currency_id integer,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: client_bills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE client_bills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: client_bills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE client_bills_id_seq OWNED BY client_bills.id;


--
-- Name: client_credits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE client_credits (
    id integer NOT NULL,
    client_id integer,
    credit_id integer,
    sum double precision,
    current_sum double precision,
    monthly_payment double precision,
    payment_date date,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: client_credits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE client_credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: client_credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE client_credits_id_seq OWNED BY client_credits.id;


--
-- Name: client_deposits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE client_deposits (
    id integer NOT NULL,
    client_id integer,
    deposit_id integer,
    sum double precision,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: client_deposits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE client_deposits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: client_deposits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE client_deposits_id_seq OWNED BY client_deposits.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE clients (
    id integer NOT NULL,
    full_name character varying,
    phone character varying,
    passport character varying,
    id_number character varying,
    email character varying,
    address character varying,
    birth date,
    sex boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: credit_operations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE credit_operations (
    id integer NOT NULL,
    client_credit_id integer,
    sum double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: credit_operations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credit_operations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credit_operations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credit_operations_id_seq OWNED BY credit_operations.id;


--
-- Name: credits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE credits (
    id integer NOT NULL,
    name character varying,
    currency_id integer,
    term integer,
    rate double precision,
    max_sum double precision,
    penalty double precision,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: credits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credits_id_seq OWNED BY credits.id;


--
-- Name: currencies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE currencies (
    id integer NOT NULL,
    name character varying,
    symbol character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE currencies_id_seq OWNED BY currencies.id;


--
-- Name: deposit_operations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deposit_operations (
    id integer NOT NULL,
    client_deposit_id integer,
    sum double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: deposit_operations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deposit_operations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deposit_operations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE deposit_operations_id_seq OWNED BY deposit_operations.id;


--
-- Name: deposits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE deposits (
    id integer NOT NULL,
    name character varying,
    currency_id integer,
    term integer,
    rate double precision,
    widthdrawal boolean,
    refill boolean,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: deposits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE deposits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: deposits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE deposits_id_seq OWNED BY deposits.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    login character varying,
    password_digest character varying,
    remember_token character varying,
    client_id integer,
    status integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: bill_operations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bill_operations ALTER COLUMN id SET DEFAULT nextval('bill_operations_id_seq'::regclass);


--
-- Name: client_bills id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_bills ALTER COLUMN id SET DEFAULT nextval('client_bills_id_seq'::regclass);


--
-- Name: client_credits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_credits ALTER COLUMN id SET DEFAULT nextval('client_credits_id_seq'::regclass);


--
-- Name: client_deposits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_deposits ALTER COLUMN id SET DEFAULT nextval('client_deposits_id_seq'::regclass);


--
-- Name: clients id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: credit_operations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_operations ALTER COLUMN id SET DEFAULT nextval('credit_operations_id_seq'::regclass);


--
-- Name: credits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits ALTER COLUMN id SET DEFAULT nextval('credits_id_seq'::regclass);


--
-- Name: currencies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY currencies ALTER COLUMN id SET DEFAULT nextval('currencies_id_seq'::regclass);


--
-- Name: deposit_operations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY deposit_operations ALTER COLUMN id SET DEFAULT nextval('deposit_operations_id_seq'::regclass);


--
-- Name: deposits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY deposits ALTER COLUMN id SET DEFAULT nextval('deposits_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: bill_operations bill_operations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bill_operations
    ADD CONSTRAINT bill_operations_pkey PRIMARY KEY (id);


--
-- Name: client_bills client_bills_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_bills
    ADD CONSTRAINT client_bills_pkey PRIMARY KEY (id);


--
-- Name: client_credits client_credits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_credits
    ADD CONSTRAINT client_credits_pkey PRIMARY KEY (id);


--
-- Name: client_deposits client_deposits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_deposits
    ADD CONSTRAINT client_deposits_pkey PRIMARY KEY (id);


--
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: credit_operations credit_operations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_operations
    ADD CONSTRAINT credit_operations_pkey PRIMARY KEY (id);


--
-- Name: credits credits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits
    ADD CONSTRAINT credits_pkey PRIMARY KEY (id);


--
-- Name: currencies currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (id);


--
-- Name: deposit_operations deposit_operations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deposit_operations
    ADD CONSTRAINT deposit_operations_pkey PRIMARY KEY (id);


--
-- Name: deposits deposits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY deposits
    ADD CONSTRAINT deposits_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_bill_operations_on_client_bill_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bill_operations_on_client_bill_id ON bill_operations USING btree (client_bill_id);


--
-- Name: index_client_credits_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_client_credits_on_client_id ON client_credits USING btree (client_id);


--
-- Name: index_client_deposits_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_client_deposits_on_client_id ON client_deposits USING btree (client_id);


--
-- Name: index_clients_on_full_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_clients_on_full_name ON clients USING btree (full_name);


--
-- Name: index_clients_on_id_number; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_clients_on_id_number ON clients USING btree (id_number);


--
-- Name: index_clients_on_passport; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_clients_on_passport ON clients USING btree (passport);


--
-- Name: index_credit_operations_on_client_credit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_credit_operations_on_client_credit_id ON credit_operations USING btree (client_credit_id);


--
-- Name: index_deposit_operations_on_client_deposit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_deposit_operations_on_client_deposit_id ON deposit_operations USING btree (client_deposit_id);


--
-- Name: index_users_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_client_id ON users USING btree (client_id);


--
-- Name: index_users_on_login; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_login ON users USING btree (login);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20161121132310'), ('20161121132312'), ('20161121132313'), ('20161121132315'), ('20161121132317'), ('20161121132319'), ('20161121132321'), ('20161121132322'), ('20161121132324'), ('20161121132326'), ('20161204124355');


