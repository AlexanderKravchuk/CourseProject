class CreateClientBills < ActiveRecord::Migration[5.0]
  def change
    create_table :client_bills do |t|
      t.integer :client_id
      t.float :sum
      t.integer :currency_id
      t.boolean :status

      t.timestamps
    end
    add_index :client_bills, :client_id
  end
end
