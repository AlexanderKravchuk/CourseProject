class CreateCredits < ActiveRecord::Migration[5.0]
  def change
    create_table :credits do |t|
      t.string :name, unique: true
      t.integer :currency_id
      t.integer :term
      t.float :rate
      t.float :max_sum
      t.float :penalty
      t.boolean :status

      t.timestamps
    end
  end
end
