class CreateDeposits < ActiveRecord::Migration[5.0]
  def change
    create_table :deposits do |t|
      t.string :name, unique: true
      t.integer :currency_id
      t.integer :term
      t.float :rate
      t.boolean :widthdrawal
      t.boolean :refill
      t.boolean :status


      t.timestamps
    end
  end
end
