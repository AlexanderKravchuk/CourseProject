class CreateBillOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :bill_operations do |t|
      t.integer :client_bill_id
      t.float :sum

      t.timestamps
    end
    add_index :bill_operations, :client_bill_id
  end
end
