class CreateCurrencies < ActiveRecord::Migration[5.0]
  def change
    create_table :currencies do |t|
      t.string :name, unique: true
      t.string :symbol, unique: true

      t.timestamps
    end
  end
end
