class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :login
      t.string :password_digest
      t.string :remember_token
      t.integer :client_id
      t.integer :status

      t.timestamps
    end
    add_index :users, :login, unique: true
    add_index :users, :client_id, unique: true
  end
end
